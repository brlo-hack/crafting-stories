/* eslint-disable no-param-reassign */
import Vue from 'vue';
import Vuex from 'vuex';
import ideasList from '@/data/ideasList';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    ideasList,
  },
  mutations: {
    addIdea(state, payload) {
      state.ideasList.unshift(payload);
    },
  },
});

/* eslint-enable no-param-reassign */
export default store;
