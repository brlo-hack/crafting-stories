import Vue from 'vue';
import Router from 'vue-router';
import IdeasList from '@/pages/IdeasList';
import IdeasDetails from '@/pages/IdeasDetails';
import NewIdea from '@/pages/NewIdea';
import IdeaApplication from '@/pages/IdeaApplication';
import ApplicationSuccess from '@/pages/ApplicationSuccess';

import '@/styles/index.scss';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ideas-list',
      component: IdeasList,
    },
    {
      path: '/ideas-details/:id',
      name: 'ideas-details',
      component: IdeasDetails,
    },
    {
      path: '/new-idea',
      name: 'new-idea',
      component: NewIdea,
    },
    {
      path: '/idea-application',
      name: 'idea-application',
      component: IdeaApplication,
    },
    {
      path: '/application-success',
      name: 'application-success',
      component: ApplicationSuccess,
    },
  ],
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});
